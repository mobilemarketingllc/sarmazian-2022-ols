msgid ""
msgstr ""
"Project-Id-Version: FacetWP (front)\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-02-21 14:01+0000\n"
"PO-Revision-Date: 2021-10-19 12:17+0000\n"
"Last-Translator: Matt Gibbs <hello@facetwp.com>\n"
"Language-Team: Polish\n"
"Language: pl_PL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 "
"|| n%100>14) ? 1 : 2);\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.5.0; wp-5.6\n"
"X-Poedit-SourceCharset: UTF-8\n"

#: includes/facets/rating.php:110
msgid "& up"
msgstr "& się"

#: includes/facets/fselect.php:34 includes/facets/fselect.php:71
#: includes/facets/dropdown.php:33 includes/facets/hierarchy.php:25
msgid "Any"
msgstr "Dowolny"

#: includes/facets/date_range.php:303
msgid "Clear"
msgstr "Wyczyść"

#: includes/facets/proximity.php:180
msgid "Clear location"
msgstr "Wyczyść lokalizację"

#: includes/facets/date_range.php:22
msgid "Date"
msgstr "Data"

#: includes/class-renderer.php:588
msgid "Date (Newest)"
msgstr "Daty (Najnowsze)"

#: includes/class-renderer.php:595
msgid "Date (Oldest)"
msgstr "Daty (Najstarsze)"

#: includes/facets/proximity.php:309
msgid "Distance"
msgstr "Odległość"

#: includes/facets/date_range.php:28
msgid "End Date"
msgstr "Data zakończenia"

#: includes/facets/search.php:19
msgid "Enter keywords"
msgstr "Wprowadź słowa kluczowe"

#: includes/facets/proximity.php:66
msgid "Enter location"
msgstr "Wprowadź lokalizację"

#: includes/facets/autocomplete.php:169
msgid "Enter {n} or more characters"
msgstr ""

#: includes/integrations/woocommerce/woocommerce.php:466
msgid "Featured"
msgstr ""

#: includes/facets/number_range.php:31 includes/facets/autocomplete.php:60
msgid "Go"
msgstr "Idź"

#: includes/integrations/woocommerce/woocommerce.php:449
msgid "In Stock"
msgstr "W magazynie"

#: includes/facets/autocomplete.php:168
msgid "Loading"
msgstr ""

#: includes/facets/number_range.php:28
msgid "Max"
msgstr "Max"

#: includes/facets/number_range.php:25
msgid "Min"
msgstr "Min"

#: includes/integrations/acf/acf.php:290
msgid "No"
msgstr "Nie"

#: includes/facets/autocomplete.php:93 includes/facets/autocomplete.php:170
msgid "No results"
msgstr "Brak wyników"

#: includes/class-display.php:151 includes/facets/fselect.php:78
msgid "No results found"
msgstr "Nie znaleziono żadnych wyników"

#: includes/facets/number_range.php:22
msgid "Number"
msgstr "Numer"

#: includes/class-renderer.php:536
msgid "of"
msgstr "z"

#: includes/integrations/woocommerce/woocommerce.php:457
msgid "On Sale"
msgstr "W sprzedaży"

#: includes/integrations/woocommerce/woocommerce.php:449
msgid "Out of Stock"
msgstr "Brak w magazynie"

#: includes/class-renderer.php:661
msgid "Per page"
msgstr "Na stronę"

#: includes/facets/slider.php:20
msgid "Reset"
msgstr "Resetuj"

#: includes/facets/fselect.php:77
msgid "Search"
msgstr "Szukaj"

#: includes/facets/checkboxes.php:130 includes/facets/hierarchy.php:160
msgid "See less"
msgstr "Zobacz mniej"

#: includes/facets/hierarchy.php:159
msgid "See more"
msgstr "Zobacz więcej"

#: includes/facets/checkboxes.php:129
msgid "See {num} more"
msgstr "Zobacz {num} więcej"

#: includes/class-renderer.php:570
msgid "Sort by"
msgstr "Sortuj wg"

#: includes/facets/date_range.php:25
msgid "Start Date"
msgstr "Data rozpoczęcia"

#: includes/facets/autocomplete.php:57
msgid "Start typing"
msgstr "Zacznij wpisywać"

#: includes/class-renderer.php:574
msgid "Title (A-Z)"
msgstr "Nazwy (A-Z)"

#: includes/class-renderer.php:581
msgid "Title (Z-A)"
msgstr "Nazwy (Z-A)"

#: includes/facets/rating.php:111
msgid "Undo"
msgstr "Cofnij"

#: includes/integrations/acf/acf.php:290
msgid "Yes"
msgstr "Tak"

#: includes/facets/fselect.php:76
msgid "{n} selected"
msgstr "wybrano {n}"
